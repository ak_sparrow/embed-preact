import { FunctionalComponent } from "preact";
import { useEffect, useRef, useState } from "preact/hooks";
import { EmbedItem } from "../types";
import { CarouselItem } from "./CarouselItem";
import Glide, {
  Controls,
  Breakpoints,
  // eslint-disable-next-line
  // @ts-ignore
} from "@glidejs/glide/dist/glide.modular.esm";

export const Carousel: FunctionalComponent<{
  data: EmbedItem[];
}> = ({ data }) => {
  const carouselContainer = useRef<HTMLDivElement>(null);
  const [activeSlide, setActiveSlide] = useState(1);

  useEffect(() => {
    const glide = new Glide(carouselContainer.current, {
      perView: 3,
      focusAt: "center",
      gap: 10,
      startAt: 1,
      animationDuration: 300,
    }).mount({
      Controls,
      Breakpoints,
    });

    glide.on("run.before", (e: { steps: string }) => {
      setActiveSlide(Number(e.steps));
    });
  }, []);

  return (
    <div className="ss-embed-carousel" ref={carouselContainer}>
      <div className="ss-embed__track glide__track" data-glide-el="track">
        <ul className="ss-embed__slides glide__slides">
          {data.map((item, i) => {
            return <li
              key={i}
              className={`ss-embed__slide glide__slide ${
                activeSlide === i
                  ? "glide__slide--pre-active"
                  : i === activeSlide - 1
                  ? "glide__slide--previous"
                  : i === activeSlide + 1
                  ? "glide__slide--next"
                  : ""
              }`}
            >
              <CarouselItem data={item} />
            </li>;
          })}
        </ul>
      </div>
      <div className="glide__bullets bullets" data-glide-el="controls[nav]">
        {data.map((item, i) => {
          return <button
            key={i}
            className="glide__bullet bullet"
            data-glide-dir={`=${i}`}
          />;
        })}
      </div>
    </div>
  );
};
