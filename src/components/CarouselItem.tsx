import { FunctionalComponent } from "preact";
import { Quote } from "../icons/Quote";
import { EmbedItem } from "../types";
import { Stars } from "./Stars";

export const CarouselItem: FunctionalComponent<{ data: EmbedItem }> = ({
  data,
}) => {
  const { avatar, name, rating, review } = data;

  return (
    <div className="ss-embed-review__item item">
      <Quote />
      <p className="review">{review}</p>
      <div className="ss-embed-review__item-footer footer">
        <div className="ss-embed-review__avatar avatar">
          <img src={avatar} alt={`${name} profile picture`} />
        </div>
        <div className="ss-embed-review__info info">
          <p>{name}</p>
          <Stars rating={rating} />
        </div>
      </div>
    </div>
  );
};
