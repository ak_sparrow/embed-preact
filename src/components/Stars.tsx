import { FunctionalComponent } from "preact";
import { Star } from "../icons/Star";

export const Stars: FunctionalComponent<{ rating?: number }> = ({
  rating = 1,
}) => {
  const fillArray = Array.from({ length: 5 });

  return (
    <div class="ss-embed-review__stars">
      {fillArray.map((_, i) => (
        <Star color={i < rating ? "#F5C744" : "#CCCDCE"} key={i} />
      ))}
    </div>
  );
};
