export interface EmbedItem {
  review: string;
  name: string;
  avatar: string;
  rating: number;
}