import { FunctionalComponent } from "preact";

export const Star: FunctionalComponent<{ size?: number; color?: string }> = ({
  size = 32,
  color = "#fff",
}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      fill="none"
      viewBox="0 0 19 19"
    >
      <g clip-path="url(#clip0_90_1252)">
        <path
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="1.5"
          d="M5.647 15.794a.909.909 0 01-1.325-.954l.657-3.803-2.783-2.695a.908.908 0 01.506-1.547l3.847-.555 1.72-3.463a.916.916 0 011.638 0l1.718 3.463 3.848.555a.908.908 0 01.506 1.547l-2.785 2.693.657 3.803a.91.91 0 01-1.325.956L9.086 14l-3.44 1.794z"
        />
      </g>
      <defs>
        <clipPath id="clip0_90_1252">
          <path fill="#fff" d="M0 0H18.171V18.171H0z" />
        </clipPath>
      </defs>
    </svg>
  );
};
