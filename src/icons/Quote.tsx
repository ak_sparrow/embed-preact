import { FunctionalComponent } from "preact";

export const Quote: FunctionalComponent<{ size?: number; color?: string }> = ({
  size = 32,
  color = "#fff",
}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size / 1.29}
      fill="none"
      viewBox="0 0 35 27"
    >
      <path
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M22.186 13.5h9.506c1.05 0 1.9.887 1.9 1.98v7.916c0 1.092-.85 1.979-1.9 1.979h-7.605c-1.05 0-1.901-.887-1.901-1.98V13.5zM22.186 13.5c0-7.917 1.425-9.235 5.703-11.875M1.272 13.5h9.506c1.05 0 1.901.887 1.901 1.98v7.916c0 1.092-.851 1.979-1.9 1.979H3.172c-1.05 0-1.901-.887-1.901-1.98V13.5zM1.272 13.5c0-7.917 1.426-9.235 5.704-11.875"
      />
    </svg>
  );
};
