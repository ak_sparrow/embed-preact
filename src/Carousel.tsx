import { FunctionalComponent } from "preact";
import { Carousel } from "./components/Carousel";
import "./style/embed.css";
import "@glidejs/glide/dist/css/glide.core.css";

export const CarouselPage: FunctionalComponent = () => {
  const data = [
    {
      review:
        "SurveySparrow is helping us obtain insight into our leads' and customers' perceptions of our products and our company - and that is very important to us.",
      avatar: "https://via.placeholder.com/200/200",
      name: "John Wick",
      rating: 3,
    },
    {
      review:
        "SurveySparrow is helping us obtain insight into our leads' and customers' perceptions of our products and our company - and that is very important to us.",
      avatar: "https://via.placeholder.com/200/200",
      name: "John Wick",
      rating: 3,
    },
    {
      review:
        "SurveySparrow is helping us obtain insight into our leads' and customers' perceptions of our products and our company - and that is very important to us.",
      avatar: "https://via.placeholder.com/200/200",
      name: "John Wick",
      rating: 3,
    },
    {
      review:
        "SurveySparrow is helping us obtain insight into our leads' and customers' perceptions of our products and our company - and that is very important to us.",
      avatar: "https://via.placeholder.com/200/200",
      name: "John Wick",
      rating: 3,
    },
  ];

  return (
    <main>
      <div id="preact_root">
        <Carousel data={data} />
      </div>
    </main>
  );
};
